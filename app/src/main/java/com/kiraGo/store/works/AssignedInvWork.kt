package com.kiraGo.store.works

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.room.FtsOptions
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.kiraGo.store.GoApplication
import com.kiraGo.store.MainActivity
import com.kiraGo.store.Repos.MainRepository
import com.kiraGo.store.api.ApiCalls
import com.kiraGo.store.api.GetInvResp
import com.kiraGo.store.db.AssignedInvDAO
import com.kiraGo.store.db.CustomerTable
import com.kiraGo.store.db.OrderDetails
import com.kiraGo.store.di.DaggerAppComponent
import com.kiraGo.store.utils.App
import com.kiraGo.store.utils.NotiUtil
import com.kiraGo.store.vo.AssignData
import org.jetbrains.anko.custom.async
import org.jetbrains.anko.doAsync
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject

class AssignedInvWork(context: Context, params: WorkerParameters)
    : Worker(context, params) {
    val TAG = "AssignedInvWork"

    @Inject
    lateinit var apiCalls: ApiCalls

    @Inject
    lateinit var dao: AssignedInvDAO

    @Inject
    lateinit var app: App

    override fun doWork(): Result {

        if (applicationContext is GoApplication) {
            var daggerAppComponent = DaggerAppComponent.builder().application(applicationContext as GoApplication).build()
            daggerAppComponent.injectInAssignedInvWork(this)
        }
        try {
            val data = inputData.getString("data")
            if (data!!.isEmpty()) {
                return Result.success()
            }
            val cal: Call<GetInvResp>? = apiCalls?.getInvoices(app.getUserD().access_token, data)
            val response: Response<GetInvResp>? = cal?.execute()
            if (response?.code() == 200) {
                if (response.body()?.status!!) {

                    response.body()!!.assign_data.forEach {

                        doAsync {
                            dao.insertOrders(it.order)
                            it.orderDetails.forEach {
                                dao.insertOrderDetails(it)
                            }
                            //dao.insetCustomer(it.customer)
                            it.productz.forEach { dao.insetProduct(it) }
                        }
                    }

                    val i = Intent(applicationContext, MainActivity::class.java)
                    NotiUtil.sendNotification("${response.body()?.assign_data!!.size} New Order to Deliver",
                            " invoices assigned to delivery",
                            i, applicationContext, 20)

                    /* val intent = Intent(applicationContext, MainActivity::class.java)
                     intent.putExtra("tag", "AssignGoUser")
                     LocalBroadcastManager.getInstance(context).sendBroadcast(intent)*/
                    return Result.success()
                } else {
                    return Result.retry()
                }
            } else {
                return Result.retry()
            }
        } catch (e: Exception) {
            Log.e(TAG, "exp", e)
            return Result.retry()
        }

    }
}