package com.kiraGo.store.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "orders")
data class OrderTable(@PrimaryKey
                      @ColumnInfo(name = "id")
                      var id: Long = 0,
                      @ColumnInfo(name = "customer_id")
                      var customer_id: String = "",
                      @ColumnInfo(name = "order_date")
                      var order_date: Date = Date(),
                      @ColumnInfo(name = "order_amount")
                      var order_amount: String = "",
                      @ColumnInfo(name = "discount")
                      var discount: String = "",
                      @ColumnInfo(name = "net_amount")
                      var net_amount: String = "",
                      @ColumnInfo(name = "udhaar")
                      var udhaar: String = "",
                      @ColumnInfo(name = "loyalty_points_used")
                      var loyalty_points_used: String = "",
                      @ColumnInfo(name = "parent_id")
                      var parent_id: String = "",
                      @ColumnInfo(name = "delivery_charges")
                      var delivery_charges: String = "",
                      @ColumnInfo(name = "delivery_address")
                      var delivery_address: String = "",
                      @ColumnInfo(name = "assign_date")
                      var assign_date: String = "",
                      @ColumnInfo(name = "assign_status")
                      var assign_status: Int = 0,
                      @ColumnInfo(name = "cash")
                      var cash: String = "",
                      @ColumnInfo(name = "previous_udhaar_paid")
                      var previous_udhaar_paid: String = "",
                      @ColumnInfo(name = "trasaction_type")
                      var trasaction_type: String = "",
                      @ColumnInfo(name = "delivery_sign")
                      var delivery_sign: String = "",
                      @ColumnInfo(name = "online_status")
                      var online_status: String = "",
                      @Transient
                      var isRead: Boolean = false,
                      @ColumnInfo(name = "invoice_no")
                      var invoice_no: String = "") {
    constructor() : this(0, "", Date(), "", "", "", "", "", "", "", "", "", 0, "", "", "", "", "",false, "")

}

