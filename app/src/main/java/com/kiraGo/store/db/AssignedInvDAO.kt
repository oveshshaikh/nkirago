package com.kiraGo.store.db

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kiraGo.store.vo.InvoiceModel

@Dao
interface AssignedInvDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrders(vararg orderTable: OrderTable)

    @Query("SELECT * FROM ORDERS")
    fun getAllOrders(): LiveData<List<OrderTable>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insetCustomer(vararg customerTable: CustomerTable)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrderDetails(orderDetails: OrderDetails)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insetProduct(productTable: ProductTable)


    @Query("SELECT orders.id,orders.customer_id,orders.order_amount,orders.assign_date,orders.assign_status,orders.invoice_no,orders.order_date,orders.delivery_sign,orders.online_status,orders.delivery_charges,CustomerTable.c_name,CustomerTable.c_mobile_no,orders.delivery_address FROM orders,CustomerTable WHERE orders.assign_status=:status AND orders.customer_id=CustomerTable.cust_id ORDER BY id DESC")
    fun getOrdersByStatus(status: Int): LiveData<MutableList<InvoiceModel>>

    @Query("SELECT orders.id,orders.customer_id,orders.order_amount,orders.assign_date,orders.assign_status,orders.invoice_no,orders.order_date,orders.delivery_sign,orders.online_status,orders.delivery_charges,CustomerTable.c_name,CustomerTable.c_mobile_no,orders.delivery_address FROM orders,CustomerTable WHERE orders.assign_status=:status AND orders.customer_id=CustomerTable.cust_id ORDER BY id DESC")
    fun getOrdersByStatusPaged(status: Int): DataSource.Factory<Int, InvoiceModel>

    @Query("UPDATE orders SET assign_status=2,delivery_sign=:image WHERE id=:order_id")
    fun changeOrderStatusWithSignature(order_id: Long, image: String)

    @Query("SELECT ProductTable.* FROM OrderDetails INNER JOIN ProductTable ON OrderDetails.product_id=ProductTable.id WHERE OrderDetails.customer_order_id =:order_id")
    fun getProductListForOrder(order_id: Long): LiveData<List<ProductTable>>

    @Query("SELECT OrderDetails.* FROM OrderDetails WHERE OrderDetails.customer_order_id =:order_id")
    fun getOrderListForOrder(order_id: Long): LiveData<List<OrderDetails>>

    @Query("SELECT orders.id,orders.customer_id,orders.order_amount,orders.assign_date,orders.assign_status,orders.invoice_no,orders.order_date,orders.delivery_sign,orders.online_status,orders.delivery_charges,CustomerTable.c_name,CustomerTable.c_mobile_no,orders.delivery_address FROM orders,CustomerTable WHERE orders.id=:order_id AND orders.customer_id=CustomerTable.cust_id")
    fun getSingleOrderDetails(order_id: Long): LiveData<InvoiceModel>

    @Query("UPDATE orders SET assign_status=3 WHERE id=:order_id")
    fun changeOrderStatus(order_id: Long)


}