package com.kiraGo.store.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class OrderDetails(@PrimaryKey
                        @SerializedName("id")
                        var id: Long = 0,
                        @SerializedName("customer_order_id")
                        val customer_order_id: Long = 0,
                        @SerializedName("product_id")
                        val product_id: String = String(),
                        @SerializedName("quantity")
                        val quantity: String = String(),
                        @SerializedName("unit_id")
                        val unit_id: String = String(),
                        @SerializedName("cost_price")
                        val cost_price: String = String(),
                        @SerializedName("mrp")
                        val mrp: String = String(),
                        @SerializedName("priceForSelling")
                        val priceForSelling: String  ="",
                        @SerializedName("priceToConsider")
                        val priceToConsider: String = "",
                        @SerializedName("product_name")
                        val product_name: String = String())