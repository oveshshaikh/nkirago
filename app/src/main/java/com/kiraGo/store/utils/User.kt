package com.kiraGo.store.utils

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

data class User(
        @SerializedName("user_id")
        var user_id: Long,
        @SerializedName("user_mobile_no")
        var user_mobile_no: String = "",
        @SerializedName("user_name")
        var user_name: String = "",
        @SerializedName("r_shop_name")
        var r_shop_name: String = "",
        @SerializedName("created_by")
        var shopId: String = "",
        @SerializedName("r_shop_address")
        var r_shop_address: String = "",
        @SerializedName("r_shop_city")
        var r_shop_city: String = "",
        @SerializedName("r_shop_pincode")
        var r_shop_pincode: String = "",
        @SerializedName("go_user_access_token")
        var access_token: String,
        @SerializedName("go_user_refresh_token")
        var refresh_token: String,
        @SerializedName("r_full_name")
        var r_full_name: String = "") {

    constructor() : this(0, "", "", "",
            "", "", "", "", "", "")

    override fun toString(): String {
        return this.toJson()
    }

    fun toJson(): String {
        return Gson().toJson(this)
    }

    fun fromJson(json: String) = Gson().fromJson<User>(json, this.javaClass)

}