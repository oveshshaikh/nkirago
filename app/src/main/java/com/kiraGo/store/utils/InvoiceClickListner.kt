package com.kiraGo.store.utils

interface InvoiceClickListner {
    fun onInvoiceSelected(orderId: Long)
}