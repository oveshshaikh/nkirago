package com.kiraGo.store.utils

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.provider.Settings
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import android.util.TypedValue
import android.view.View
import android.widget.TextView
import com.kiraGo.store.R

/**
Created by admin on 1/29/2018.
 */


object PermissionUtils {

    private fun useRunTimePermissions(): Boolean {
        return Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1
    }

    fun hasPermission(activity: Activity, permission: String): Boolean {
        return if (useRunTimePermissions()) {
            activity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
        } else true
    }

    fun requestPermissions(activity: Activity, permission: Array<String>, requestCode: Int) {
        if (useRunTimePermissions()) {
            activity.requestPermissions(permission, requestCode)
        }
    }

    fun requestPermissions(fragment: androidx.fragment.app.Fragment, permission: Array<String>, requestCode: Int) {
        if (useRunTimePermissions()) {
            fragment.requestPermissions(permission, requestCode)
        }
    }

    fun shouldShowRational(activity: Activity, permission: String): Boolean {
        return if (useRunTimePermissions()) {
            activity.shouldShowRequestPermissionRationale(permission)
        } else false
    }

    fun goToAppSettings(v: View) {
        val bar = Snackbar.make(v, "Allow permission", Snackbar.LENGTH_LONG)
        val snackView = bar.view
        val tv = snackView.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, v.context.resources.getDimension(R.dimen.text_size_medium))
        snackView.setBackgroundColor(ContextCompat.getColor(v.context, R.color.colorPrimaryDark))
        bar.setActionTextColor(Color.WHITE)
        bar.setAction("Yes") {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.fromParts("package", it.context.packageName, null))
            intent.addCategory(Intent.CATEGORY_DEFAULT)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            it.context.startActivity(intent)
        }
        bar.show()

    }

}