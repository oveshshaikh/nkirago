package com.kiraGo.store.utils

import android.annotation.SuppressLint
import android.content.SharedPreferences
import com.kiraGo.store.di.Injectable
import com.kiraGo.store.utils.AppConstants.USER
import java.lang.Exception
import javax.inject.Inject

class App(val sharedpreferences: SharedPreferences) {


    fun getTok(key: String): String {
        return sharedpreferences.getString(key, "")
    }

    @SuppressLint("ApplySharedPref")
    fun put(key: String, value: Any, isCommit: Boolean = false) {
        val editor = sharedpreferences.edit()
        if (value is String)
            editor.putString(key, value)
        if (value is Boolean)
            editor.putBoolean(key, value)
        if (value is Int)
            editor.putInt(key, value)
        if (isCommit)
            editor.commit()
        else
            editor.apply()
    }


    fun hasKey(key: String): Boolean {
        return sharedpreferences.contains(key)
    }

    fun remove(key: String) {
        sharedpreferences.edit().remove(key).commit()
    }

    fun getUserD(): User {
        return User().fromJson(getTok(USER))
    }

    fun setUserD(userD: User) {
        put(USER, userD.toJson(), true)
    }

    fun clearPrefs() {
        sharedpreferences.edit().clear().commit()
    }
}