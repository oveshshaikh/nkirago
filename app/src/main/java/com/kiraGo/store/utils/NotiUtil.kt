package com.kiraGo.store.utils

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.kiraGo.store.R

object NotiUtil {

    fun sendNotification(title: String, message: String, i: Intent?, c: Context, notifID: Int) {

        var pendingIntent: PendingIntent? = null

        if (i != null) {
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            pendingIntent = PendingIntent.getActivity(c, 0, i,
                    PendingIntent.FLAG_ONE_SHOT)
        }

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val notificationManager: NotificationManager? = c.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager


        val channelId = "default"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId, title, NotificationManager.IMPORTANCE_DEFAULT)
            channel.description = message
            channel.enableLights(true)
            channel.lightColor = ContextCompat.getColor(c, R.color.colorPrimaryDark)
            channel.setSound(defaultSoundUri, null)
            channel.setShowBadge(true)
            notificationManager?.createNotificationChannel(channel)

            val notificationBuilder = Notification.Builder(c, channelId)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.drawable.ic_notify)
                    .setColor(ContextCompat.getColor(c, R.color.colorPrimaryDark))
                    .setSound(defaultSoundUri)
                    .setAutoCancel(true)
            if (i != null)
                notificationBuilder.setContentIntent(pendingIntent)
            notificationManager?.notify(notifID, notificationBuilder.build())
        } else {
            val notificationBuilder = NotificationCompat.Builder(c, "default")
                    .setSmallIcon(R.drawable.ic_notify)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setColor(ContextCompat.getColor(c, R.color.colorPrimaryDark))
                    .setSound(defaultSoundUri)
                    .setLights(ContextCompat.getColor(c, R.color.colorPrimaryDark), 3000, 3000)
            if (i != null)
                notificationBuilder.setContentIntent(pendingIntent)
            notificationManager?.notify(notifID, notificationBuilder.build())

        }
    }
}