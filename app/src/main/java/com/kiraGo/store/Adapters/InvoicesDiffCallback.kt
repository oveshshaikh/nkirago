package com.kiraGo.store.Adapters

import androidx.recyclerview.widget.DiffUtil
import com.kiraGo.store.vo.InvoiceModel

class InvoicesDiffCallback : DiffUtil.ItemCallback<InvoiceModel>() {
    override fun areContentsTheSame(oldItem: InvoiceModel, newItem: InvoiceModel): Boolean {
        return oldItem == newItem    }


    override fun areItemsTheSame(oldItem: InvoiceModel, newItem: InvoiceModel): Boolean {
        return oldItem.id == newItem.id
    }
}