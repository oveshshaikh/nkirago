package com.kiraGo.store.Adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.kiraGo.store.R
import com.kiraGo.store.utils.InvoiceClickListner
import com.kiraGo.store.vo.InvoiceModel

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.math.roundToInt


class InvicesAdapter(val context: Context,val invoiceClickListner: InvoiceClickListner) : PagedListAdapter<InvoiceModel, InvicesAdapter.InvoicesViewHolder>(InvoicesDiffCallback()) {

    override fun onBindViewHolder(holderInvoices: InvoicesViewHolder, position: Int) {
        var invoiceitem = getItem(position)

        if (invoiceitem == null) {
            holderInvoices.clear()
        } else {
            holderInvoices.bind(invoiceitem)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InvoicesViewHolder {
        return InvoicesViewHolder(LayoutInflater.from(context).inflate(R.layout.item_delivery,
                parent, false),invoiceClickListner)
    }


    class InvoicesViewHolder(view: View,val invoiceClickListner: InvoiceClickListner) : RecyclerView.ViewHolder(view) {

        private val ordType : AppCompatTextView
        private val ordId: AppCompatTextView
        private val amt: AppCompatTextView
        private val custName: AppCompatTextView
        private val custAdd: AppCompatTextView
        private val custPhone: AppCompatTextView
        private val ordDate: AppCompatTextView
        private val isRead: AppCompatImageView
        private var totamt : Float?= 0.2F

        val formatter = SimpleDateFormat("dd-MMMM-yyyy", Locale.ENGLISH)

        init {
            // itemView.setOnClickListener(listner)
            formatter.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
            ordType = itemView.findViewById(R.id.lbl_ord_type)
            ordId = itemView.findViewById(R.id.order_number)
            amt = itemView.findViewById(R.id.amt)
            custName = itemView.findViewById(R.id.name)
            custAdd = itemView.findViewById(R.id.add)
            custPhone = itemView.findViewById(R.id.phone)
            ordDate = itemView.findViewById(R.id.ordDate)
            isRead = itemView.findViewById(R.id.badge)
        }

        fun bind(invoiceModel: InvoiceModel) {

            if(invoiceModel.online_status.equals("1"))
                ordType.text = "Order for delivery"
            else
                ordType.text = "Order for Store pickup"

            ordId.text = invoiceModel.invoice_no

            totamt = invoiceModel.order_amount.toFloat() + invoiceModel.delivery_charges.toFloat()
            amt.text = "₹ "+totamt!!.roundToInt().toString()
            custName.text = invoiceModel.c_name
            custAdd.text = invoiceModel.delivery_address
            custPhone.text = invoiceModel.c_mobile_no
           // isRead.visibility = if (invoiceModel.isRead) View.INVISIBLE else View.VISIBLE
            val str = invoiceModel.assign_date.split(" ")
            /*Log.d("STr",str[0].toString());
            val sdf = SimpleDateFormat(invoiceModel.assign_date);*//*

            val dateInString = str.get(0)
            val date: Date = SimpleDateFormat(formatter.toString()).parse(dateInString)
            val formattedDateString: String = formatter.format(date)*/
            ordDate.text = getDate(str.get(0))
            itemView.setOnClickListener{
                invoiceClickListner.onInvoiceSelected(invoiceModel.id)
            }
        }

        fun clear() {

        }

        fun getDate(str:String) : String
        {
            var date :String ?=""
            var month:String?=null
            var dmy = str.split("-")
            Log.e("Month",dmy.get(1))
            when(dmy.get(1))
            {
                "01"-> {
                    month = "Jan"
                }
                "02"-> {
                    month = "Feb"
                }
                "03"->{
                    month = "Mar"
                }
                "04"->{
                    month = "Apr"
                }
                "05"->{
                    month ="May"
                }
                "06"->{
                    month ="Jun"
                }
                "07"->{
                    month ="Jul"
                }
                "08"->{
                    month ="Aug"
                }
                "09"->{
                    month ="Sep"
                }
                "10"->{
                    month="Oct"
                }
                "11"->{
                    month="Nov"
                }
                "12"->{
                    month="Dec"
                }
            }
            date = dmy.get(2).plus(" "+month).plus(" "+dmy.get(0))
            return date
        }
    }
    /* class AssignedViewHlder(itemView: View, listner: View.OnClickListener) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

         private val ordId: AppCompatTextView
         private val amt: AppCompatTextView
         private val custName: AppCompatTextView
         private val custAdd: AppCompatTextView
         private val custPhone: AppCompatTextView
         private val ordDate: AppCompatTextView
         private val isRead: AppCompatImageView
         private val cusrBox: Box<CustomerTable>

         init {
             itemView.setOnClickListener(listner)
             ordId = itemView.findViewById(R.id.order_number)
             amt = itemView.findViewById(R.id.amt)
             custName = itemView.findViewById(R.id.name)
             custAdd = itemView.findViewById(R.id.add)
             custPhone = itemView.findViewById(R.id.phone)
             ordDate = itemView.findViewById(R.id.ordDate)
             isRead = itemView.findViewById(R.id.badge)
             cusrBox = (itemView.context.applicationContext as GoController).boxStore.boxFor(CustomerTable::class.java)
         }

         fun bind(order: Order) {
             val cust: CustomerTable = cusrBox[order.customer_id.toLong()]
             ordId.text = order.invoice_no
             amt.text = order.order_amount
             custName.text = cust.c_name
             custAdd.text = cust.delivery_address
             custPhone.text = cust.c_mobile_no
             isRead.visibility = if (order.isRead) View.INVISIBLE else View.VISIBLE
             ordDate.text = SimpleDateFormat("dd / MMM / yyyy - HH:mm", Locale.getDefault()).format(order.order_date)
         }
     }

     class DateViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
         val ordId: AppCompatTextView = itemView.findViewById(R.id.lbl_date)*/
}