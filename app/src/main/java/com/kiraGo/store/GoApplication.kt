package com.kiraGo.store

import android.app.Activity
import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import com.facebook.stetho.Stetho
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.kiraGo.store.di.AppInjector
import com.kiraGo.store.utils.AppConstants
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector

import javax.inject.Inject

class GoApplication : Application(), HasActivityInjector {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity> {
        return dispatchingAndroidInjector
    }

    lateinit var remoteConfig: FirebaseRemoteConfig
    override fun onCreate() {
        super.onCreate()

        remoteConfig = FirebaseRemoteConfig.getInstance()
        val configSettings = FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build()
        remoteConfig.setConfigSettings(configSettings)
        /*val defaults = HashMap<String, Any>()
        defaults.put("base_url", "https://kira.store/kira/api/")
        defaults.put("signature_url", "https://kira.store/kira/uploads/deliver_signature/")
        remoteConfig.setDefaults(defaults)
        remoteConfig.fetch(0).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                remoteConfig.activateFetched()
            }
            updateLocalValues()
        }*/
        /* remoteConfig.fetch(0)
                 .addOnCompleteListener(this) { task ->
                     if (task.isSuccessful) {
                         Toast.makeText(this, "Fetch Succeeded",
                                 Toast.LENGTH_SHORT).show()

                         // After config data is successfully fetched, it must be activated before newly fetched
                         // values are returned.
                         remoteConfig.activateFetched()
                     } else {
                         Toast.makeText(this, "Fetch Failed",
                                 Toast.LENGTH_SHORT).show()
                     }
                     displayWelcomeMessage()
                 }*/


        Stetho.initializeWithDefaults(this)
        AppInjector.init(this)

    }

    private fun updateLocalValues() {
        val baseUrl = remoteConfig.getString("base_url")
        val signatureUrl = remoteConfig.getString("signature_url")

        Log.d("GOAPPLICATION", baseUrl + ", \n " + signatureUrl)
        AppConstants.BASE_URL = baseUrl
        AppConstants.SIGNATURE_URL = signatureUrl
    }

    fun inNetwork(): Boolean {
        var isConnected = false
        val manager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val nInfo = manager.activeNetworkInfo
        if (nInfo != null && nInfo.isConnected)
            isConnected = true
        return isConnected
    }
//    override fun activityInjector() = dispatchingAndroidInjector


}