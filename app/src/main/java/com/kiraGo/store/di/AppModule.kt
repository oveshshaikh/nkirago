package com.kiraGo.store.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.GsonBuilder
import com.kiraGo.store.GoApplication
import com.kiraGo.store.api.ApiCalls
import com.kiraGo.store.db.AssignedInvDAO
import com.kiraGo.store.db.KiraGoDB
import com.kiraGo.store.utils.App
import com.kiraGo.store.utils.AppConstants.BASE_URL
import com.kiraGo.store.utils.AppConstants.KIRA_GO_PREFS
import com.kiraGo.store.utils.RefTokenAuth
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class
AppModule {
    @Provides
    @Singleton
    fun provideRetofitService(refTokenAuth: RefTokenAuth): ApiCalls {
        return Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(
                        GsonConverterFactory.create(
                                GsonBuilder().setDateFormat("yyyy-mm-dd HH:mm:ss").create()))
                .client(OkHttpClient.Builder()
                        .authenticator(refTokenAuth)
                       .addNetworkInterceptor(StethoInterceptor())
                        .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                        /*.addInterceptor({ chain ->
                            val res = chain.proceed(chain.request())
                            if (res.code() == 406) {
                                Log.d("api_error", "406")
                                *//*BaseRepo.getInstance().truncateAllTables()
                                sharedpreferences.edit().clear().apply()
                                try {
                                    JobManager.instance().cancelAll()
                                } catch (a: Exception) {
                                    Log.e("JobManager", "cancelAll", a)
                                }*//*

                                *//* startActivity(Intent(context, MainActivity::class.java)
                                         .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))*//*
                            }
                            res
                        })*/
                        .build())
                .build().create(ApiCalls::class.java)
    }

    @Singleton
    @Provides
    fun provideDb(app: Application): KiraGoDB {
        return Room.databaseBuilder(app, KiraGoDB::class.java, "kira_go.db")
                .fallbackToDestructiveMigration()
                .build()
    }

    @Singleton
    @Provides
    fun provideAssignedInvDAO(db: KiraGoDB): AssignedInvDAO {
        return db.assignedInvDAO()
    }

    @Singleton
    @Provides
    fun provideSharedPrefs(app: Application): SharedPreferences {
        return app.getSharedPreferences(KIRA_GO_PREFS, Context.MODE_PRIVATE)
    }

    @Singleton
    @Provides
    fun providePrefsEditor(preferences: SharedPreferences): SharedPreferences.Editor {
        return preferences.edit()
    }

    @Singleton
    @Provides
    fun provideApp(sharedPreferences: SharedPreferences): App {
        return App(sharedPreferences)
    }

    @Provides
    fun provideAuthenticator(app: App, context: Application): RefTokenAuth {
        return RefTokenAuth(app, context)

    }

    @Singleton
    @Provides
    fun provideApplication(app: Application): GoApplication {
        return app as GoApplication
    }


/*
    @Provides
    @Singleton
    fun provideGson():GsonConverterFactory{
        return GsonConverterFactory.create(
                GsonBuilder().setDateFormat("yyyy-mm-dd HH:mm:ss").create())
    }

    @Provides
    @Singleton
    fun provideHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
                .authenticator(RefTokenAuth())
                .addNetworkInterceptor(StethoInterceptor())
                .build()
    }*/
}