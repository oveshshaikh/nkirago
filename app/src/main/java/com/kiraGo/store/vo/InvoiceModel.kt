package com.kiraGo.store.vo

import com.google.gson.annotations.SerializedName
import java.util.*

data class InvoiceModel(@SerializedName("id")
                        val id: Long = 0,
                        @SerializedName("customer_id")
                        val customer_id: String = "",
                        @SerializedName("order_amount")
                        val order_amount: String = "",
                        @SerializedName("assign_date")
                        val assign_date: String = "",
                        @SerializedName("assign_status")
                        val assign_status: Int = 0,
                        @SerializedName("invoice_no")
                        val invoice_no: String = ""
                        , @SerializedName("order_date")
                        val order_date: Date = Date()
                        ,/* @SerializedName("isRead")
                        val isRead: Boolean = false
                        ,*/
                        @SerializedName("c_name")
                        val c_name: String = "",
                        @SerializedName("c_mobile_no")
                        val c_mobile_no: String = "",
                        @SerializedName("delivery_address")
                        val delivery_address: String = "",
                        @SerializedName("delivery_sign")
                        val delivery_sign: String = "",
                        @SerializedName("online_status")
                        val online_status: String = "",
                        @SerializedName("delivery_charges")
                        val delivery_charges: String = ""
)/*
orders.id,orders.customer_id,orders.net_amount,
orders.assign_date,orders.assign_status,
orders.invoice_no,CustomerTable.c_name
,CustomerTable.c_mobile_no,CustomerTable.delivery_address */
