package com.kiraGo.store.vo

import com.google.gson.annotations.SerializedName
import com.kiraGo.store.utils.User

data class RegResp(@SerializedName("message")
                   val message: String,
                   @SerializedName("user_details")
                   val userDetails: User,
                   @SerializedName("status")
                   val status: Boolean)