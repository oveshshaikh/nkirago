package com.kiraGo.store.ui.InvoiceDetails

import android.graphics.Bitmap
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kiraGo.store.Adapters.ProductListAdp

import com.kiraGo.store.R
import com.kiraGo.store.db.OrderDetails
import com.kiraGo.store.di.Injectable
import com.kiraGo.store.utils.AppConstants
import com.kiraGo.store.utils.AppConstants.ORDER_ID
import com.kiraGo.store.utils.AppConstants.SIGNATURE_URL
import com.kiraGo.store.utils.AppConstants.TOKEN
import com.kiraGo.store.vo.InvoiceModel
import com.squareup.picasso.Picasso
import com.williamww.silkysignature.views.SignaturePad
import kotlinx.android.synthetic.main.assigned_invoices_fragment.*
import kotlinx.android.synthetic.main.invoice_details_fragment.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject
import kotlin.math.roundToInt

class InvoiceDetails : Fragment(), Injectable, View.OnClickListener, SignaturePad.OnSignedListener {

    val TAG = "InvoiceDetails"
    override fun onStartSigning() {
        close.setText(R.string.clear)
    }

    override fun onClear() {

    }

    override fun onSigned() {

    }

    override fun onClick(v: View?) {
        if (v == null)
            return
        when (v.id) {
            close.id -> {
                if (close.text == getString(R.string.clear)) {
                    signature_pad.clear()
                    close.setText(R.string.close)
                } else {
                    activity?.onBackPressed()
                }
            }
            back_btn.id -> {
                activity?.onBackPressed()
            }
            submitbutton.id -> {
                if (AppConstants.inNetwork(context!!)) {
                    progressBar2.visibility = View.VISIBLE
                    if (statusmode.checkedRadioButtonId == radio_AbsenceofReciver.id) {

                        viewModel.updateOrderStatus(TOKEN,
                                order_id,
                                RequestBody.create(MediaType.parse("text/plain"), order_number.text.toString()),
                                false,
                                MultipartBody.Part.createFormData("signature", ""))
                    } else {
                        if (signature_pad.isEmpty) {
                            Toast.makeText(context, R.string.signature_err, Toast.LENGTH_SHORT).show()
                        } else {
                            try {
                                val orderNumber=order_number.text.toString()
                                val name = order_number.text.replace(Regex("[^a-zA-Z]"), "-")
                                val file = File(activity!!.cacheDir, "$name.jpeg")
                                val fOut = FileOutputStream(file)
                                val pictureBitmap = signature_pad.signatureBitmap
                                pictureBitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut)
                                val reqBdy = RequestBody.create(MediaType.parse("*/*"), file)
                                viewModel.updateOrderStatus(TOKEN,
                                        order_id,
                                        RequestBody.create(MediaType.parse("text/plain"),orderNumber),
                                        true,
                                        MultipartBody.Part.createFormData("signature", file.name, reqBdy))
                            } catch (e: Exception) {
                                Log.e(TAG, "updateStatus", e)
                                progressBar2.visibility = View.GONE
                                Toast.makeText(context, R.string.oops, Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                } else {
                    Toast.makeText(context, R.string.net_required, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var productListAdp: ProductListAdp

    companion object {
        fun newInstance(order_id: Long): InvoiceDetails {
            val fragment = InvoiceDetails()
            val bundle = Bundle(1)
            bundle.putLong(ORDER_ID, order_id)
            fragment.arguments = bundle
            return fragment
        }
    }

    var order_id: Long = 0
    var itemSize : Int ?=0
    var itemQty : Int ?=0
    var listAdp : List<OrderDetails> ?=null
    var amt1 :Float = 0.2F
    private lateinit var viewModel: InvoiceDetailsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.invoice_details_fragment, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(InvoiceDetailsViewModel::class.java)
        // viewModel.changeorderStatusLocally(order_id!!)
        viewModel.getSingleOrderDetail(order_id).observe(this, Observer {
            setUpView(it)
        })
        /*viewModel.getProductsforOrder(order_id).observe(this, Observer {
            productListAdp.addProducts(it)
        })*/

        viewModel.getOrderListforOrder(order_id).observe(this, Observer {
            productListAdp.addProducts(it)
            itemSize = it.size
            listAdp = it
            setItem()
        })


        viewModel.isLoading.observe(this, Observer<Boolean> {
            it?.let { showLoadingDialog(it) }
        })
        viewModel.apiError.observe(this, Observer<Throwable> {
            it?.let {
                Log.d(TAG, "APi error" + it.message)
                Toast.makeText(context, getString(R.string.oops), Toast.LENGTH_SHORT).show()
            }
            //Snackbar.make(context, it.localizedMessage, Snackbar.LENGTH_LONG).show() }
        })
        viewModel.updateStat.observe(this, Observer {
            Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
            activity!!.onBackPressed()
        })

    }

    private fun setItem(){

        bill_items.text = "Items :"+ itemSize

        //bill_amount.text = invoiceModel.delivery_charges

        listAdp?.forEach {
            itemQty = itemQty?.plus(it.quantity.toInt())
        }

        bill_quantity.text = "Qty :"+itemQty
    }

    private fun setUpView(invoiceModel: InvoiceModel) {

        if(invoiceModel.online_status.equals("1"))
            lbl_ord_type.text = "Order for delivery"
        else
            lbl_ord_type.text = "Order for store pickup"
        order_number.text = invoiceModel.invoice_no
        //amt.text = invoiceModel.order_amount
        name.text = invoiceModel.c_name
        add.text = invoiceModel.delivery_address
        phone.text = invoiceModel.c_mobile_no
        toolbar_invoice.text=invoiceModel.invoice_no

        bill_sub_total.text=invoiceModel.order_amount
        bill_delivery.text =invoiceModel.delivery_charges

        amt1 = invoiceModel.order_amount.toFloat() + invoiceModel.delivery_charges.toFloat()
        bill_amount.text = "Amt :₹"+ amt1.roundToInt()
        amt.text = "₹ "+amt1.roundToInt().toString()

        back_btn.setOnClickListener(this)
        if (invoiceModel.assign_status == 2) {
            signatureImageView.visibility = View.VISIBLE
            signature_pad.visibility = View.GONE
            controls.visibility = View.GONE
            for (i in 0 until statusmode.childCount)
                statusmode.getChildAt(i).isEnabled = false

            for (i in 0 until paymentmode.childCount)
                paymentmode.getChildAt(i).isEnabled = false


            Picasso.get()
                    .load(SIGNATURE_URL + invoiceModel.delivery_sign)
                    .error(android.R.drawable.stat_notify_error)
                    .into(signatureImageView)
        } else {
            close.setOnClickListener(this)
            submitbutton.setOnClickListener(this)
            signature_pad.setOnSignedListener(this)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        order_id = arguments?.getLong(ORDER_ID, 0) ?: 0
        productListAdp = ProductListAdp(arrayListOf())
        orderItems.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        orderItems.adapter = productListAdp
    }

    private fun showLoadingDialog(show: Boolean) {
        if (show) {
            progressbar.visibility = View.VISIBLE
        } else {
            progressbar.visibility = View.GONE
        }
    }
}
