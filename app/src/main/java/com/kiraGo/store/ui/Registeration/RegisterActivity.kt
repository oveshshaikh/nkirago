package com.kiraGo.store.ui.Registeration

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.TransitionDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.google.zxing.Result
import com.kiraGo.store.MainActivity
import com.kiraGo.store.R
import com.kiraGo.store.di.Injectable
import com.kiraGo.store.ui.InvoiceDetails.InvoiceDetailsViewModel
import com.kiraGo.store.utils.App
import com.kiraGo.store.utils.AppConstants
import com.kiraGo.store.utils.AppConstants.USER
import com.kiraGo.store.utils.PermissionUtils
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_register.*
import me.dm7.barcodescanner.zxing.ZXingScannerView
import javax.inject.Inject

class RegisterActivity : AppCompatActivity(), ZXingScannerView.ResultHandler, View.OnClickListener, TextView.OnEditorActionListener, Animation.AnimationListener, Injectable, HasSupportFragmentInjector {


    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return dispatchingAndroidInjector
    }


    private lateinit var context: Context
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var app: App

    private lateinit var viewModel: RegisterViewModel
    val TAG = "RegisterActivity"
    override fun onClick(v: View?) {
        when (v?.id) {
            start.id -> {
                if (PermissionUtils.hasPermission(this, Manifest.permission.CAMERA)) {
                    val anim = AnimationUtils.loadAnimation(context, android.R.anim.slide_out_right)
                    anim.setAnimationListener(this)
                    logo_lay.startAnimation(anim)
                } else {
                    /* if (PermissionUtils.shouldShowRational(this, Manifest.permission.CAMERA)) {
                         PermissionUtils.requestPermissions(this,
                                 arrayOf(Manifest.permission.CAMERA), 10)
                     } else {
                         PermissionUtils.goToAppSettings(v)
                     }*/
                    /*PermissionUtils.requestPermissions(thisA,
                            arrayOf(Manifest.permission.CAMERA), 10)
                 */   ActivityCompat.requestPermissions(this,
                            arrayOf(Manifest.permission.CAMERA),
                            10)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(RegisterViewModel::class.java)

        context = this
        scan.setResultHandler(this)
        start.setOnClickListener(this)
        code.setBackgroundColor(Color.TRANSPARENT)
        code.setOnEditorActionListener(this)


        viewModel.isLoading.observe(this, Observer<Boolean> {
            it?.let { Log.d(TAG, "LOADING") }
        })
        viewModel.apiError.observe(this, Observer<Throwable> {
            it?.let {
                Log.d(TAG, "APi error" + it.message)
                Toast.makeText(context, getString(R.string.oops), Toast.LENGTH_SHORT).show()
                scan.resumeCameraPreview(this)
            }
            //Snackbar.make(context, it.localizedMessage, Snackbar.LENGTH_LONG).show() }
        })
        viewModel.regResp.observe(this, Observer {
            Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
            // app.setUserD(it.userDetails)

            val anim = AnimationUtils.loadAnimation(context, android.R.anim.fade_out)

            anim.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationRepeat(animation: Animation?) {
                }

                override fun onAnimationStart(animation: Animation?) {
                }

                override fun onAnimationEnd(animation: Animation?) {
                    scan.visibility = View.GONE
                    code.visibility = View.GONE

                    val arr = arrayOf(ContextCompat.getDrawable(context, android.R.color.white),
                            ContextCompat.getDrawable(context, R.color.colorPrimary))
                    val crossfader = TransitionDrawable(arr)
                    root.background = crossfader
                    crossfader.startTransition(500)

/*
                    FirebaseInstanceId.getInstance().instanceId
                            .addOnSuccessListener { instanceIdResult ->
                                val deviceToken = instanceIdResult.token
                                if (app.getTok(Constantz.FCM_TOKEN) != deviceToken)
                                    SendFcmKey.scheduleJob(deviceToken, app.inNetwork())
                            }*/
                    Handler().postDelayed({
                        val sucAnim = AnimationUtils.loadAnimation(context, R.anim.test)
                        sucAnim.setAnimationListener(object : Animation.AnimationListener {
                            override fun onAnimationRepeat(animation: Animation?) {

                            }

                            override fun onAnimationEnd(animation: Animation?) {
                                val intent = Intent(context, MainActivity::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                                startActivity(intent)
                            }

                            override fun onAnimationStart(animation: Animation?) {
                                Toast.makeText(context, "Welcome " + it.userDetails.user_name,
                                        Toast.LENGTH_LONG).show()
                            }

                        })
                        text.visibility = View.VISIBLE
                        succ.visibility = View.VISIBLE
                        text.startAnimation(AnimationUtils.loadAnimation(context, android.R.anim.fade_in))
                        succ.startAnimation(sucAnim)
                    }, 500)
                }
            })
            scan.startAnimation(anim)
            Snackbar.make(start, it.message, BaseTransientBottomBar.LENGTH_SHORT).show()
        })

    }

    override fun onAnimationRepeat(animation: Animation?) {

    }

    override fun onAnimationEnd(animation: Animation?) {
        scan.startCamera()
        logo_lay.visibility = View.GONE
        scan.visibility = View.VISIBLE
        code.visibility = View.VISIBLE
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId == EditorInfo.IME_ACTION_GO) {
            if (v == null)
                return false
            if (v.text.length >= 5)
                handleTxt(v.text.toString())
            else
                Snackbar.make(logo_lay, "Invalid Code", BaseTransientBottomBar.LENGTH_SHORT).show()
        }
        return false
    }

    override fun onAnimationStart(animation: Animation?) {

    }

    override fun handleResult(value: Result?) {
        if (value != null && value.text.length > 5)
            handleTxt(value.text)
        else
            Snackbar.make(logo_lay, "Invalid Code", BaseTransientBottomBar.LENGTH_SHORT).show()
    }

    private fun handleTxt(txt: String) {
        // val app = GoController.instance
        if (AppConstants.inNetwork(context)) {
            /*   val api = GoController.retrofit?.create(Callz::class.java)
               api?.reg(txt)?.enqueue(this)*/
            viewModel.registrerUser(txt)
        } else {
            scan.resumeCameraPreview(this)
            Snackbar.make(logo_lay, "Internet Required", BaseTransientBottomBar.LENGTH_SHORT).show()
        }
    }

    override fun onPause() {
        super.onPause()
        scan.stopCamera()
    }

    override fun onResume() {
        super.onResume()
        if (logo_lay.visibility == View.GONE)
            scan.startCamera()
    }

    override fun onBackPressed() {
        if (logo_lay.visibility == View.VISIBLE) {
            super.onBackPressed()
        } else {
            logo_lay.visibility = View.VISIBLE
            val ani = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left)
            ani.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationRepeat(animation: Animation?) {

                }

                override fun onAnimationEnd(animation: Animation?) {
                    scan.stopCamera()
                    scan.visibility = View.GONE
                    code.visibility = View.GONE
                }

                override fun onAnimationStart(animation: Animation?) {

                }
            })
            logo_lay.startAnimation(ani)
        }
    }



    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode==10){
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                start.performClick()
            }else{
                PermissionUtils.goToAppSettings(root)
            }
        }
    }

}
