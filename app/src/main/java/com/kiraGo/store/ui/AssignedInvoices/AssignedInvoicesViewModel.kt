package com.kiraGo.store.ui.AssignedInvoices

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.kiraGo.store.Repos.MainRepository
import com.kiraGo.store.vo.InvoiceModel
import javax.inject.Inject

class AssignedInvoicesViewModel @Inject constructor(val repository: MainRepository) : ViewModel() {
    // TODO: Implement the ViewModel
    //lateinit var assignedInvoices: MutableLiveData<List<OrderTable>>


    var isLoading = MutableLiveData<Boolean>()
    var isLoggingOut = MutableLiveData<Boolean>()
    var apiError = MutableLiveData<Throwable>()

    private var invoicesLiveData: LiveData<PagedList<InvoiceModel>>? = null


    fun subsribetoLiveInvoicesForStatus(status: Int) {
        /* val factory: DataSource.Factory<Int, InvoiceModel> =
                 repository.getInvoiceAsPerStatusPaged(status)

         val pagedListBuilder: LivePagedListBuilder<Int, InvoiceModel> = LivePagedListBuilder<Int, InvoiceModel>(factory,
                 50)
         invoicesLiveData = pagedListBuilder.build()*/


        invoicesLiveData =
                LivePagedListBuilder(
                        repository.getInvoiceAsPerStatusPaged(status), /* page size */ 20).build()
    }


    /*init {
        val factory: DataSource.Factory<Int, InvoiceModel> =
                repository.getInvoiceAsPerStatusPaged(0)

        val pagedListBuilder: LivePagedListBuilder<Int, InvoiceModel> = LivePagedListBuilder<Int, InvoiceModel>(factory,
                50)
        invoicesLiveData = pagedListBuilder.build()
    }*/

    fun getInvoicesLiveData() = invoicesLiveData


    fun getInvoicesfromWeb() {
        isLoading.value = true
        repository.getAssignedInvoices(
                {
                    //invoicesLiveData.value = it
                    //getliveInvoicelist(1)
                    isLoading.value = false
                },

                {

                    isLoading.value = false
                    apiError.value = it
                })
    }

    fun getliveInvoicelist(status: Int): LiveData<MutableList<InvoiceModel>> {
        return repository.getliveInvoices(status)
    }

    fun logout() {
        repository.logoutGoUser({
            Log.e("logoutGoUser", it.toString())
            isLoggingOut.value = true
        },
                {
                    isLoggingOut.value = false
                })
    }

}
