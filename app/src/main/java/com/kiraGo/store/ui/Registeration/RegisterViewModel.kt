package com.kiraGo.store.ui.Registeration

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kiraGo.store.Repos.MainRepository
import com.kiraGo.store.utils.User
import com.kiraGo.store.vo.RegResp
import javax.inject.Inject

class RegisterViewModel @Inject constructor(val repository: MainRepository) : ViewModel() {
    var isLoading = MutableLiveData<Boolean>()

    var apiError = MutableLiveData<Throwable>()
    var regResp = MutableLiveData<RegResp>()

    fun registrerUser(verificationCode: String) {
        repository.registerUser(verificationCode, {
            isLoading.value = false
            regResp.value=it
        }, {
            apiError.value=it
            isLoading.value = false
        })
    }
}