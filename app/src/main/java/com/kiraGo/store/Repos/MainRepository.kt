package com.kiraGo.store.Repos

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import com.kiraGo.store.api.ApiCalls
import com.kiraGo.store.api.GetInvResp
import com.kiraGo.store.db.AssignedInvDAO
import com.kiraGo.store.db.KiraGoDB
import com.kiraGo.store.db.OrderDetails
import com.kiraGo.store.db.ProductTable
import com.kiraGo.store.utils.App
import com.kiraGo.store.utils.EmptyInvoicesException
import com.kiraGo.store.vo.InvoiceModel
import com.kiraGo.store.vo.RegResp
import com.kiraGo.store.vo.UpdateStat
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.jetbrains.anko.doAsync
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MainRepository @Inject constructor(val apicalls: ApiCalls, val dao: AssignedInvDAO, val app: App) {
    val TAG = "MainRepository"

    @Inject
    lateinit var db: KiraGoDB

    fun getInvoiceAsPerStatusPaged(status: Int): DataSource.Factory<Int, InvoiceModel> {
        return dao.getOrdersByStatusPaged(status)
    }

    /*fun getLiveInvoices(status: Int): LiveData<List<InvoiceModel>> {
        return dao.getOrdersByStatus(status)
    }
*/

    fun getAssignedInvoices(successHandler: (Boolean?) -> Unit, failureHandler: (Throwable?) -> Unit) {
        apicalls.getAllInvoices(app.getUserD().access_token).enqueue(object : Callback<GetInvResp> {
            override fun onResponse(call: Call<GetInvResp>, response: Response<GetInvResp>) {

                if (response?.code() == 200 && response.body()!!.status) {
                    response.body()!!.assign_data.forEach {
                        doAsync {

                            Log.d("orders",it.order.order_amount);
                            dao.insertOrders(it.order)
                            it.orderDetails.forEach {
                                dao.insertOrderDetails(it)
                            }
                            dao.insetCustomer(it.customer)
                            it.productz.forEach { dao.insetProduct(it) }
                        }
                    }

                    successHandler(true)
                    failureHandler(null)
                } else {
                    failureHandler(EmptyInvoicesException("No Invoices"))
                }
            }

            override fun onFailure(call: Call<GetInvResp>?, t: Throwable?) {
                failureHandler(t)
            }
        })
    }

    fun changeOrderstatus(order_id: Long, image: String) {
        doAsync {
            dao.changeOrderStatusWithSignature(order_id, image)
        }
    }

    fun getSingleOrderDetails(order_id: Long): LiveData<InvoiceModel> {
        return dao.getSingleOrderDetails(order_id)
    }

    fun getProductListforOrder(order_id: Long): LiveData<List<ProductTable>> {
        return dao.getProductListForOrder(order_id)
    }

    fun getOrderListforOrder(order_id: Long): LiveData<List<OrderDetails>> {
        return dao.getOrderListForOrder(order_id)
    }

    fun updateStatusofOrder(access_token: String, invoiceId: Long, invoiceNo: RequestBody, isComplete: Boolean, signature: MultipartBody.Part, successHandler: (UpdateStat) -> Unit, failureHandler: (Throwable?) -> Unit) {
        apicalls.updateStatus(app.getUserD().access_token,
                RequestBody.create(MediaType.parse("text/plain"), invoiceId.toString()),
                invoiceNo,
                RequestBody.create(MediaType.parse("text/plain"), isComplete.toString()),
                signature).enqueue(object : Callback<UpdateStat> {
            override fun onFailure(call: Call<UpdateStat>, t: Throwable) {
                failureHandler(t)
            }

            override fun onResponse(call: Call<UpdateStat>, response: Response<UpdateStat>) {
                if (response?.code() == 200) {
                    if (response.body()!!.status) {
                        doAsync {
                            if (isComplete) {
                                dao.changeOrderStatusWithSignature(invoiceId, response.body()!!.image)

                            } else {
                                dao.changeOrderStatus(invoiceId)
                            }
                        }

                    }
                    successHandler(response.body()!!)

                } else
                    failureHandler(Throwable())
            }

        })
    }


    fun registerUser(verificationCode: String, successHandler: (RegResp) -> Unit, failureHandler: (Throwable?) -> Unit) {

        apicalls.reg(verificationCode).enqueue(object : Callback<RegResp> {
            override fun onFailure(call: Call<RegResp>, t: Throwable) {
                failureHandler(t)
            }

            override fun onResponse(call: Call<RegResp>, response: Response<RegResp>) {
                val resp: RegResp? = response.body()
                if (response.code() == 200 && resp != null) {
                    app.setUserD(resp.userDetails)
                    successHandler(resp)
                } else {
                    failureHandler(Throwable())
                }

            }

        })
    }

    fun getliveInvoices(status: Int): LiveData<MutableList<InvoiceModel>> = dao.getOrdersByStatus(status)

    fun logoutGoUser(successHandler: (Boolean?) -> Unit, failureHandler: (Throwable?) -> Unit) {
        apicalls.logout(app.getUserD().access_token, app.getUserD().user_id).enqueue(object : Callback<RegResp> {
            override fun onFailure(call: Call<RegResp>, t: Throwable) {
                failureHandler(t)
            }

            override fun onResponse(call: Call<RegResp>, response: Response<RegResp>) {

                try {
                    if (response.body()!!.status) {
                        doAsync {
                            db.clearAllTables()
                            app.clearPrefs()

                        }
                        successHandler(true)
                    }
                }
                catch (ex: Exception)
                {
                    doAsync {
                        db.clearAllTables()
                        app.clearPrefs()

                    }
                    successHandler(true)
                }
            }
        })
    }

}