package com.kiraGo.store.api

import com.google.gson.annotations.SerializedName
import com.kiraGo.store.vo.AssignData
import java.util.*


data class GetInvResp(@SerializedName("status")
                      val status: Boolean,
                      @SerializedName("assign_data")
                      val assign_data: List<AssignData>)